import java.util.Date;

public class Order {

    private final String description;// lauks description
    private final Customer customer;

    private final Date submisionDate;

    public Order(String orderDescription, Customer customer, Date submisionDate) {
        this.description = orderDescription;
        this.customer = customer;
        this.submisionDate = submisionDate;
        customer.add(this);
    }

    public String toString() {
        return customer.getInfo() + " " + description.toString() + " " + submisionDate.toString();
    }

    public boolean isAfter(Date from) {
        return submisionDate.compareTo(from) >= 0;
    }

}
