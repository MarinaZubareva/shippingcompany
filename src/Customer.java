import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Stream;

public class Customer {

    private String name;
    private int customerId;

    private static Map<Integer, Customer> id2Customer = new HashMap<>();

    private List<Order> orderList;


    public Customer(int customerId, String Name) {
        this.name = Name;
        this.customerId = customerId;
        orderList = new ArrayList<>();
    }

    public void add(Order item) {
        orderList.add(item);
    }

    public String toString() {
        return name + orderList.toString();
    }

    public String getInfo() {
        return name + " " + orderList.size();
    }



    private static void readCustomersFromFile(String customersFile) throws IOException {
        ClassLoader loader = Customer.class.getClassLoader();
        InputStream inputStream = loader.getResourceAsStream(customersFile);
        try (Stream<String> stream = new BufferedReader(new InputStreamReader(inputStream)).lines()) {
            stream.forEach(line -> getCustomer(line));
        } catch (Exception e) {
            e.printStackTrace();
        }

/*        final Customer customer = new Customer();
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(file)))) {
             id2Customer = reader.lines().map(line -> getCustomer(customer, line))
                     .collect(Collectors.toMap());
        }*/

    }

    private static void readOrdersFromFile(String file) throws IOException {
        //  try (BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(file)))) {
        //orderList = reader.lines().map(line -> getOrder(formatter, line)).collect(Collectors.toList());
        ClassLoader loader = Customer.class.getClassLoader();
        InputStream inputStream = loader.getResourceAsStream(file);
        try (Stream<String> stream = new BufferedReader(new InputStreamReader(inputStream)).lines()) {
            stream.forEach(line -> getOrder(line));
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private static Customer getCustomer(String line) {
        String[] arrayFromLine = line.split("\t");
        int idCustomer = Integer.valueOf(arrayFromLine[0]);
        Customer customer = new Customer(idCustomer, arrayFromLine[1]);
        id2Customer.put(idCustomer, customer);
        return customer;
    }

    private static Date getDate(SimpleDateFormat formatter, String line) {
        try {
            return formatter.parse(line);
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }
    }

    private static Order getOrder(String line) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        try {
            String[] arrayFromLine = line.split("\t");
            Date date = formatter.parse(arrayFromLine[2]);
            return new Order(arrayFromLine[0], id2Customer.get(Integer.valueOf(arrayFromLine[1])), date);
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }
    }
    public static void main(String[] args) {
        try {
            readCustomersFromFile("customers.txt");
            readOrdersFromFile("orders.txt");
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println(id2Customer);

    }

}

//lauks string name
//Arraylist no orderiem
//japievieno orderus